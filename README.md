# Flectra Community / account-invoice-reporting

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[account_invoice_line_report](account_invoice_line_report/) | 2.0.1.0.0| New view to manage invoice lines information
[account_invoice_report_hide_line](account_invoice_report_hide_line/) | 2.0.1.0.0| Hide invoice lines from the PDF report if the unit price is 0
[account_comment_template](account_comment_template/) | 2.0.2.0.0| Comments templates on invoice documents
[account_invoice_report_due_list](account_invoice_report_due_list/) | 2.0.1.2.0| Show multiple due data in invoice
[account_invoice_production_lot](account_invoice_production_lot/) | 2.0.1.0.1| Display delivered serial numbers in invoice
[account_invoice_report_grouped_by_picking](account_invoice_report_grouped_by_picking/) | 2.0.1.1.2| Print invoice lines grouped by picking
[account_invoice_line_sale_line_position](account_invoice_line_sale_line_position/) | 2.0.1.0.0| Adds the related sale line position on invoice line.


